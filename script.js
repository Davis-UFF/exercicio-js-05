var num = [];

var intervalo = prompt("Digite um intervalo para a geração dos números aleatórios da lista." + "\n" + "A lista será preenchida com valores maiores ou iguais a 0 e menores que o intervalo definido, ou seja, 0 >= x < intervalo." + "\n" + "Exemplo : para preencher a lista com valores aleatórios entre 0 e 9, digite 10 no intervalo, pois o 10 não será considerado.");

var nova = [];

for(i=0; i<5; i++) {
  num.push(Math.floor((Math.random() * intervalo)))
};

var copia = num.slice();

alert("A lista gerada é" + "\n" + "[" + num + "]");

for(i=0; i<5; i++){
  if(copia[i] != 0 && copia[i] % 3 == 0 && copia[i] % 5 == 0){
    copia[i] = "fizzbuzz";
    nova.push(copia[i]);
  }

  else if(copia[i] != 0 && copia[i] % 3 == 0){
    copia[i] = "fizz";
    nova.push(copia[i]);
  }
  else if(copia[i] != 0 && copia[i] % 5 == 0){
    copia[i] = "buzz";
    nova.push(copia[i]);
  }
  
  else{
    copia[i] = " ";
    nova.push(copia[i]);
  }
};


alert("A lista gerada foi" + "\n" + "[" + num + "]" + "\n" + "A lista com os códigos de divibilidade é" + "\n" + "[" + nova + "]");